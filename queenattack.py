def queensAttack(n, k, r_q, c_q, obstacles):
    #moves on each scanline
    moves_up = -1
    moves_down = -1
    moves_left = -1
    moves_right = -1
    moves_ul = -1
    moves_ur = -1
    moves_dl = -1
    moves_dr = -1


    #sort obstacles
    for i in range(len(obstacles)):
        #access row in iteration
        row = obstacles[i][0]
        #access col in iteration
        col = obstacles[i][1]
        #vertical distance from queen to edge
        ver = abs(row - r_q) - 1
        #horizontal distance from queen to edge
        hor = abs(col - c_q) - 1


        #if vertical obstacle:
        if c_q == col:
            #if obstacle is above queen
            if r_q < row:
                if moves_up < 0:
                    moves_up = ver
                else:
                    moves_up = min(moves_up, ver)
            #if obstacle is below queen
            else:
                if moves_down < 0:
                    moves_down = ver
                else:
                    moves_down = min(moves_down, ver)
        #if no vertical obstacle



        #if horizontal obstacle
        if r_q == row:
            #check if obstacle is to the left of queen
            if c_q > col:
                if moves_left < 0:
                    moves_left = hor
                else:
                    moves_left = min(moves_left, hor)
            #if obstacle is to the right of queen
            else:
                if moves_right < 0:
                    moves_right = hor
                else:
                    moves_right = min(moves_right, hor)


        #diagonal obstacle  
        #obstacle on left diagonal
        if row + col == r_q + c_q:
            #upleft diagonal
            if row > r_q:
                #TODO
                #calculate distance to queen.
                if moves_ul < 0:
                    moves_ul = min(hor, ver)
                else:
                    moves_ul = min(moves_ul, min(hor, ver))

            #downright diagonal
            if row < r_q:
                #TODO
                if moves_dr < 0:
                    moves_dr = min(hor, ver)
                else:
                    moves_dr = min(moves_dr, min(hor, ver))
                #if move < distance
                #move = distance

        #right diagonal obstacle
        if row - col == r_q - c_q:
            #upright diagonal
            if row > r_q:
                #TODO
                if moves_ur < 0:
                    moves_ur = min(hor, ver)
                else:
                    moves_ur = min(moves_ur, min(hor, ver))
                #if move < distance
                #move = distance
            #downright diagonal
            if row < r_q:
                #TODO
                if moves_dl < 0:
                    moves_dl = min(hor, ver)
                else:
                    moves_dl = min(moves_dl, min(hor, ver))
                #move = distance
    if moves_right < 0:
        moves_right = n - c_q
    if moves_left < 0:
        moves_left = c_q - 1
    if moves_up < 0:
        moves_up = n - r_q
    if moves_down < 0:
        moves_down = r_q - 1
    if moves_ur < 0:
        moves_ur = min(n - c_q, n - r_q)
    if moves_ul < 0:
        moves_ul = min(c_q - 1, n - r_q)
    if moves_dr < 0:
        moves_dr = min(n - c_q, r_q - 1)
    if moves_dl < 0:
        moves_dl = min(c_q - 1, r_q - 1)

                
    return moves_right + moves_left + moves_up + moves_down + moves_ur + moves_ul + moves_dr + moves_dl